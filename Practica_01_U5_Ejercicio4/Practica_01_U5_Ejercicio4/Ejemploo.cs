﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_01_U5_Ejercicio4
{
    class Ejemploo
    {
        public int a;

        public void CalculaDivision(int numerador, int denominador)
        {
            if (denominador == 0)
                throw new Exception("El denominador NO debe ser cero");
            else
                a = (numerador / denominador);
        }
    }
}
