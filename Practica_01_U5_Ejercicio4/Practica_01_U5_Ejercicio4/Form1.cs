﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_01_U5_Ejercicio4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int a, b, c = 0;

            a = int.Parse(txtA.Text);
            b = int.Parse(txtB.Text);
            try
            {
                Ejemploo OBJ = new Ejemploo();
                OBJ.CalculaDivision(a, b);
                c = OBJ.a;

            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message);
            }
            finally
            {
                labRESULTADO.Text = (" = " + c);
            }   
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtA.Clear();
            txtB.Clear();
            labRESULTADO.Text = "RESULTADO";
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
