﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_unidad_4_Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            Metodo1 p = new Metodo1();
            Console.WriteLine("La suma de 5+10 es:" + p.Sumar(5, 10));
            Console.WriteLine("La concatenacion de \"Juan\" y \" Carlos\" es " + p.Sumar("Juan", " Carlos"));
            Console.ReadKey();

            Metodo2 v = new Metodo2
                ();
            v.Mostrar("Hola Mundo");
            v.Mostrar("Hola Mundo", 30, 10);
            v.Mostrar("Hola Mundo", 30, 12, ConsoleColor.Red);
            v.Mostrar("Hola Mundo", 30, 14, ConsoleColor.Red, ConsoleColor.Blue);
            Console.ReadKey();
        }
    }
}
