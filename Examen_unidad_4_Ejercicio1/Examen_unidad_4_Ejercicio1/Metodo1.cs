﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_unidad_4_Ejercicio1
{
    class Metodo1
    {
        public int Sumar(int x1, int x2)
        {
            int s = x1 + x2;
            return s;
        }

        public string Sumar(string s1, string s2)
        {
            string s = s1 + s2;
            return s;
        }
    }
}
