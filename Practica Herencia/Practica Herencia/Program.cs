﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Hijo obj = new Hijo();
            obj.metodo1();
            obj.AccesoMetodo3();

            Herencia obj2 = new Herencia();
            obj2.AccesoMetodo2();

            Console.ReadKey();

        }
    }
}
