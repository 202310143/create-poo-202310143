﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_Herencia
{
    class Herencia
    {
        public int atributo1;
        private int atributo2;
        protected int atributo3;


        public void metodo1() 
        {
            Console.WriteLine("Este es el Metodo 1 de Herencia");
        }

        private void metodo2()
        {
            Console.WriteLine("Este metodo no se puede acceder fuera de la clase Herencia");
        }

        protected void metodo3()
        {
            Console.WriteLine("Este es el Metodo 3 de Herencia Protected");
        }

        public void AccesoMetodo2()
        {
            metodo2();
        }
    }

    class Hijo:Herencia
    { 
        public void AccesoMetodo3()
        {
            metodo3();
        }
    }
}
