﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio5
{
    class Program
    {
        static void Main(string[] args)
        {
            NumeroMayor n = new NumeroMayor();
            short[] a = new short[20];
            long[] b = new long[20];

            n.rellenar(a);

            n.rellenar(b);

            n.detectarNomayor(a);

            n.detectarNomayor(b);
            Console.ReadKey();
        }
    }
}
