﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjemploArreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] CatalogoNumeros = new int[10];

            for(int i=0;i<CatalogoNumeros.Length;i++)
            {
                Console.WriteLine("Ingresa un numero");
                int num = Convert.ToInt32(Console.ReadLine());
                CatalogoNumeros[i] = num;
            }

            foreach (int val in CatalogoNumeros)
            {
                Console.WriteLine(val);
            }
            
            Console.ReadKey();
        }
    }
}
