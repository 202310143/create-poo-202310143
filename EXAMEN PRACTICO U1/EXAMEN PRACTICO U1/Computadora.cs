﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EXAMEN_PRACTICO_U1
{
    class Computadora
    {
        public int Monitor = 0;
        public int CPU = 0;
        public string TargetaDeVideo = "";
        public int Ram = 0;
        public int Procesador = 0;
        public string FuenteDePoder = "";
        public int TargetaMadre = 0;
        public string Ventiladores = "";
        public int Gabinete = 0;
        public int Teclado = 0;
        public int Mouse = 0;
        public string DiscoDuro = "";

        public void setMonitor(int Monitor) {
            this.Monitor = Monitor;
        }
        public int getMonitor() {
            return this.Monitor;
        }

        public void setCPU(int CPU) {
            this.CPU = CPU;
        }
        public int getCPU() {
            return this.CPU;
        }

        public void setTargetaDeVideo(string TargetaDeVideo) {
            this.TargetaDeVideo = TargetaDeVideo;
        }   
        public string getTargetaDeVideo() {
            return this.TargetaDeVideo;
        }

        public void setRam(int Ram) {
            this.Ram = Ram;
        }
        public int getRam() {
            return this.Ram;
        }

        public void setProcesador(int Procesador) {
            this.Procesador = Procesador;
        }
        public int getProcesador() {
            return this.Procesador;
        }

        public void setFuenteDePoder(string FuenteDePoder) {
            this.FuenteDePoder = FuenteDePoder;
        }
        public string getFuenteDePoder() {
            return this.FuenteDePoder;
        }

        public void setTargetaMadre(int TargetaMadre) {
            this.TargetaMadre = TargetaMadre;
        }
        public int getTargetaMadre() {
            return this.TargetaMadre;
        }

        public void setVentiladores(string Ventiladores) {
            this.Ventiladores = Ventiladores;
        }
        public string getVentiladores() {
            return this.Ventiladores;
        }

        public void setGabinete(int Gabinete) {
            this.Gabinete = Gabinete;
        }
        public int getGabinete() {
            return this.Gabinete;
        }

        public void setTeclado(int Teclado) {
            this.Teclado = Teclado;
        }
        public int getTeclado() {
            return this.Teclado;
        }

        public void setMouse(int Mouse) {
            this.Mouse = Mouse;
        }
        public int getMouse() {
            return this.Mouse;
        }

        public void setDiscoDuro(string DiscoDuro) {
            this.DiscoDuro = DiscoDuro;
        }
        public string getDiscoDuro() {
            return this.DiscoDuro;
        }
    }
}
