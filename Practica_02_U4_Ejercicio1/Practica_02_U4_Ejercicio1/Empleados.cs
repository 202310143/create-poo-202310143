﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_02_U4_Ejercicio1
{
    public abstract class Empleados
    {
        public double SalarioSemanal;
        public double Sueldo;
        public double SueldoBase;
        public double SueldoporHoras;
        public double VentasBrutas;

        public abstract void CalculaSalario(double SA, double SH, int HT);
    }
}
