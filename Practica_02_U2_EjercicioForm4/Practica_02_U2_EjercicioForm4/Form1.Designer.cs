﻿
namespace Practica_02_U2_EjercicioForm4
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Dolares = new System.Windows.Forms.Label();
            this.Pesos = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Dolares
            // 
            this.Dolares.AutoSize = true;
            this.Dolares.Location = new System.Drawing.Point(177, 105);
            this.Dolares.Name = "Dolares";
            this.Dolares.Size = new System.Drawing.Size(43, 13);
            this.Dolares.TabIndex = 0;
            this.Dolares.Text = "Dolares";
            // 
            // Pesos
            // 
            this.Pesos.AutoSize = true;
            this.Pesos.Location = new System.Drawing.Point(448, 105);
            this.Pesos.Name = "Pesos";
            this.Pesos.Size = new System.Drawing.Size(36, 13);
            this.Pesos.TabIndex = 1;
            this.Pesos.Text = "Pesos";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(280, 97);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(333, 196);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Presiona";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Pesos);
            this.Controls.Add(this.Dolares);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Dolares;
        private System.Windows.Forms.Label Pesos;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
    }
}

