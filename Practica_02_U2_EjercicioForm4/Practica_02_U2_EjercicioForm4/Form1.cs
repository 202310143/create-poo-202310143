﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_02_U2_EjercicioForm4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            metodo();
        }
        void metodo()
        {
            double dolares = Convert.ToDouble(Dolares.Text);
            dolares = dolares * 20;
            Pesos.Text = dolares.ToString();
        }

    }
}
