﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_01_U4_Ejercicio1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form fmr = null;
            switch (cboSalarios.SelectedIndex)
            {
                case 0:
                    fmr = new FormConserje();
                    break;

                case 1:
                    fmr = new FormProduccion();
                    break;

                case 2:
                    fmr = new FormAdministrativo();
                    break;

                case 3:
                    fmr = new FormGerencia();
                    break;

                case 4:
                    fmr = new FormDueños();
                    break;

            }
            Hide();
            fmr?.ShowDialog();
            Show();
            fmr.Dispose();
        }
        private void cboSalarios_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboSalarios_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
