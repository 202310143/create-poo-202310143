﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_unidad_4_Ejercicio3
{
	class Perro : IAnimal
	{
		string nombre;
		public Perro(string n)
		{
			nombre = n;
		}

		public string Hablar(string mensaje)
		{
			return nombre + " dice " + mensaje;
		}

		public void Correr()
		{
			Console.WriteLine(nombre + " corre!!!");
		}
	}
}
