﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_unidad_4_Ejercicio3
{
	public class Program
	{
		static int Main()
		{
			Perro fido = new Perro("Fido");
			fido.Correr();
			Console.WriteLine(fido.Hablar("Guau!!"));
			return 0;
		}
	}
}
