﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            Variables c = new Variables();

            Console.WriteLine("suma(10,4)");

            Console.WriteLine("multiplicacion(15,35,23)");

            Console.WriteLine("promedio(10,12,12,18)");

            Console.WriteLine();

            c.Grande(10, 4);
            c.Grande(15, 35, 23);
            c.Grande(10, 12, 12, 18);
            Console.ReadKey();
        }
    }
}
