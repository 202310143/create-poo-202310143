﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio1
{
    class Variables
    {
        public void Grande(int a, int b)
        {
            int suma = a + b;

            Console.WriteLine("suma: " + suma);
        }
        public void Grande(int a, int b, int c)
        {
            int multiplicacion = a * b * c;

            Console.WriteLine("multiplicacion: " + multiplicacion);
        }
        public void Grande(int a, int b, int c, int d)
        {
            int promedio = (a + b + c + d) / 4;

            Console.WriteLine("promedio: " + promedio);
        }
    }
}
