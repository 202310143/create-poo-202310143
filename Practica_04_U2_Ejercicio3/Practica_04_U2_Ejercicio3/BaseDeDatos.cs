﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio3
{
    class BaseDeDatos
    {
        public void LlenarDatos(String[] n)
        {
            for (int i = 0; i < 35; i++)
            {
                Console.WriteLine("ingresa nombre " + (i + 1) + ":");
                n[i] = Console.ReadLine();
            }
        }
        public void LlenarDatos(float[] t)
        {
            for (int j = 0; j < 35; j++)
            {
                Console.WriteLine("ingresa telefono " + (j + 1) + ":");
                t[j] = int.Parse(Console.ReadLine());
            }
        }
        public void imprimirdatos(String[] nombres, float[] telefonos)
        {
            for (int i = 0; i < 35; i++)
            {
                Console.Write("nombre: " + nombres[i] + " tel: " + telefonos[i]);
                Console.WriteLine("");
            }
        }
    }
}
