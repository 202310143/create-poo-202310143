﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] nombres = new String[35];
            float[] telefonos = new float[35];


            BaseDeDatos bd = new BaseDeDatos();
            bd.LlenarDatos(nombres);
            bd.LlenarDatos(telefonos);
            bd.imprimirdatos(nombres, telefonos);
            Console.ReadKey();
        }
    }
}
