﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio4
{
    class Metodoburbuja
    {
        public void ordenar(int[] edad)
        {
            int T = 0;
            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j < 9 - 1; j++)
                {
                    if (edad[j] > edad[j + 1])
                    {
                        T = edad[j];
                        edad[j] = edad[j + 1];
                        edad[j + 1] = T;
                    }
                }
            }
            for (int i = 1; i < edad.Length; i++)
            {
                Console.WriteLine(edad[i]);
            }
        }
        public void ordenar(float[] edad)
        {
            float T = 0;
            for (int i = 1; i < 10; i++)
            {
                for (int j = 1; j < 9; j++)
                {
                    if (edad[j] > edad[j + 1])
                    {
                        T = edad[j];
                        edad[j] = edad[j + 1];
                        edad[j + 1] = T;
                    }
                }
            }
            for (int i = 1; i < edad.Length; i++)
            {
                Console.WriteLine(edad[i]);
            }
        }

        public void mostrarNormal(int[] n)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(n[i]);
            }
        }
        public void mostrarNormal(float[] n)
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(n[i]);
            }
        }
    }
}
