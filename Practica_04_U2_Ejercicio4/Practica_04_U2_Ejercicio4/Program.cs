﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
                float[] lista1 = new float[10];
                lista1[0] = 8.8f;
                lista1[1] = 5.5f;
                lista1[2] = 2.3f;
                lista1[3] = 9.4f;
                lista1[4] = 7.7f;
                lista1[5] = 6.43f;
                lista1[6] = 1;
                lista1[7] = 3.2f;
                lista1[8] = 4.44f;
                lista1[9] = 9;
                int[] lista2 = new int[10];
                lista2[0] = 8;
                lista2[1] = 5;
                lista2[2] = 2;
                lista2[3] = 9;
                lista2[4] = 7;
                lista2[5] = 6;
                lista2[6] = 1;
                lista2[7] = 3;
                lista2[8] = 4;
                lista2[9] = 9;

                Metodoburbuja m = new Metodoburbuja();

                m.mostrarNormal(lista2);


                m.ordenar(lista2);

                m.mostrarNormal(lista1);


                m.ordenar(lista1);
                Console.ReadKey();
        }
    }
}
