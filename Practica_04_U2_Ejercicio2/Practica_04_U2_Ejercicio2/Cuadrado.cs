﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio2
{
    class Cuadrado
    {
        public void elevar(int n)
        {


            Console.WriteLine("   " + n + "² = " + Math.Pow(n, 2));
            Console.WriteLine("   tipo int");

            public void elevar(float n)
            {

                Console.WriteLine("   " + n + "² = " + Math.Pow(n, 2));
                Console.WriteLine("   tipo float");

            }
            public void elevar(double n)
            {

                Console.WriteLine("   " + n + "² = " + Math.Pow(n, 2));
                Console.WriteLine("   tipo double");

            }
        }
    }
}
