﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            Cuadrado ec = new Cuadrado();
            ec.elevar(5);
            ec.elevar(5.34f);
            ec.elevar(23.2d);
            Console.ReadKey();
        }
    }
}
