﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_01_U3_Ejercicio0
{
    class Punto2D
    {
        public int X;
        public int Y;
    }
    class Punto3D : Punto2D
    {
        public int Z;
    }
}
