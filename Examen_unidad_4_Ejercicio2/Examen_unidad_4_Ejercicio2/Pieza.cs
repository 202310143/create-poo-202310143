﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_unidad_4_Ejercicio2
{
        public abstract class Pieza
        {
            public abstract decimal Area();
            public abstract decimal Perimetro();
            public bool EjemploMetodo()
            {
                return false;
            }

            public int ValorNatural = 1;
        }
}
