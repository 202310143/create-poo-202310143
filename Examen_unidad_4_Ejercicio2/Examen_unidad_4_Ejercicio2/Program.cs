﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_unidad_4_Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            Cuadrado p = new Cuadrado(3);
            Console.WriteLine("Numero del area de cuadrado :" + p.Area());
            Console.WriteLine("Numero del perimetro de cuadrado :" + p.Perimetro());
            Console.ReadKey();
        }
    }
}
