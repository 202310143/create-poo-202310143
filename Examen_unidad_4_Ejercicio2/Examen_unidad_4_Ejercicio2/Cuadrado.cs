﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen_unidad_4_Ejercicio2
{
        public class Cuadrado : Pieza
        {
            readonly decimal Lado;
            public Cuadrado(decimal lado)
            {
                Lado = lado;
            }
            public override decimal Area()
            {
                return Lado * Lado;
            }

            public override decimal Perimetro()
            {
                return Lado * 4;
            }
        }
}
