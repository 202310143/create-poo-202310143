﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenCorte_U3_CarrilloSalazar
{
    class PrimeraClase
    {
        public int atributo1;
        private int atributo2;
        protected int atributo3;


        public void metodo1()
        {
            Console.WriteLine("Este es el Metodo 1 Public ");
        }

        private void metodo2()
        {
            Console.WriteLine("Este metodo no se puede acceder");
        }

        protected void metodo3()
        {
            Console.WriteLine("Este es el Metodo 3 protected");
        }

        public void AccesoMetodo2()
        {
            metodo2();
        }
    }
        
    class Hijo:PrimeraClase
    {
        public void AccesoMetodo3()
        {
            metodo3();
        }
    }
}
