﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio6
{
    class Class
    {
        public int ceros = 0;
        public int negativos = 0;
        public int positivos = 0;
        public int suman = 0;
        public int sumap = 0;
        public void contador(int[] n)
        {
            Random r = new Random();
            for (int i = 0; i < 300; i++)
            {
                n[i] = r.Next(-30, 30);
            }
        }
        public void zero(int[] n)
        {
            for (int i = 0; i < 300; i++)
            {
                if (n[i] == 0)
                {
                    ceros++;
                }
            }
        }
        public void negativo(int[] n)
        {
            for (int i = 0; i < 300; i++)
            {
                if (n[i] < 0)
                {
                    negativos++;
                    suman += n[i];
                }
            }
        }
        public void positivo(int[] n)
        {
            for (int i = 0; i < 300; i++)
            {
                if (n[i] > 0)
                {
                    positivos++;
                    sumap += n[i];
                }
            }

        }

        public void verArreglo(int[] n)
        {
            int iterator = 0;
            for (int i = 0; i < 300; i++)
            {
                if (n[i] == 0)
                {

                    if (iterator < 30)
                    {
                        Console.Write(" " + n[i] + "");
                        iterator++;
                    }
                    else
                    {
                        Console.WriteLine(" " + n[i] + "");
                        iterator = 0;
                    }

                }
                else
                {
                    if (iterator < 30)
                    {
                        Console.Write(" " + n[i] + "");
                        iterator++;
                    }
                    else
                    {
                        Console.WriteLine(" " + n[i] + "");
                        iterator = 0;
                    }
                }
            }
        }       }
}
