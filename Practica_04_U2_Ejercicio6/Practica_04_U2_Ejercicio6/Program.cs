﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_04_U2_Ejercicio6
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] n = new int[300];
            Class c = new Class();
            c.contador(n);
            c.zero(n);
            c.positivo(n);
            c.negativo(n);
            Console.WriteLine("ceros totales: {0}", c.ceros);
            Console.WriteLine("negativos totales: {0}", c.negativos);
            Console.WriteLine("positivos totales: {0}", c.positivos);
            Console.WriteLine("suma total de los numeros negativos: {0}", c.suman);
            Console.WriteLine("suma total de los numeros positivos: {0}", c.sumap);


            Console.ReadKey();
        }
    }
}
