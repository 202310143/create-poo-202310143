﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_02_U2_EjercicioConsola4
{
    class Promedio
    {
        int Cal1;
        int Cal2;
        int Cal3;
        int promedio;

        public void LeerPromedio()
        {
            Console.WriteLine("Ingrese su primera califiacion");
            Cal1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su segunda califiacion");
            Cal2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese su tercera califiacion");
            Cal3 = Convert.ToInt32(Console.ReadLine());
            promedio = (Cal1 + Cal2 + Cal3) / 3;
        }
        public void MostrarPromedio()
        {
            Console.WriteLine("El promedio de {0}", promedio);
        }
    }
}
