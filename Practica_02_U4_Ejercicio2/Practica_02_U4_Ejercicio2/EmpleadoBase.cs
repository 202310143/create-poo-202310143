﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_02_U4_Ejercicio2
{
    public class EmpleadoBase : Empleados
    {
        public override void CalculaSalario(double SA, double SH, int HT)
        {
            Sueldo = (int)((SA * SH) + HT);
        }
    }
}
